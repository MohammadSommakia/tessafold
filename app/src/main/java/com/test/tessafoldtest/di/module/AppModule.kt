package com.test.tessafoldtest.di.module

import android.app.Application
import androidx.room.Room
import com.google.gson.Gson
import com.test.tessafoldtest.data.local.AppDatabase
import com.test.tessafoldtest.utils.Const
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.Dispatchers
import javax.inject.Singleton
import kotlin.coroutines.CoroutineContext


@Module
class AppModule {

    @Singleton
    @Provides
    fun provideDatabase(
        app: Application
    ) = Room.databaseBuilder(
        app,
        AppDatabase::class.java,
        Const.DATABASE_NAME
    ).build()

    @Singleton
    @Provides
    fun providePostsDao(db: AppDatabase) = db.postsDao()

    @Singleton
    @Provides
    fun provideCommentsDao(db: AppDatabase) = db.commentsDao()

    @Singleton
    @Provides
    fun provideUsersDao(db: AppDatabase) = db.usersDao()


    @Provides
    @Singleton
    fun provideCoroutineContext(): CoroutineContext {
        return Dispatchers.Main
    }

    @Provides
    @Singleton
    fun provideGsonObject(): Gson {
        return Gson()
    }
}