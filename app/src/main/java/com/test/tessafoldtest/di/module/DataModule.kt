package com.test.tessafoldtest.di.module

import com.test.tessafoldtest.data.repository.DataRepository
import com.test.tessafoldtest.data.repository.DataSource
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class DataModule {
    @Binds
    @Singleton
    abstract fun provideDataRepository(dataRepository: DataRepository): DataSource
}