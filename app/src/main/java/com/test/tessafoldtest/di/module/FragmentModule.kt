package com.test.tessafoldtest.di.module

import com.test.tessafoldtest.presentation.comments.CommentsFragment
import com.test.tessafoldtest.presentation.post.PostsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
internal abstract class FragmentModule
{

    @ContributesAndroidInjector
    abstract fun contributePostsFragment(): PostsFragment

    @ContributesAndroidInjector
    abstract fun contributeCommentsFragment(): CommentsFragment

}