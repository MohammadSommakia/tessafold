package com.test.tessafoldtest

import android.app.Application
import com.test.tessafoldtest.di.component.AppComponent
import com.test.tessafoldtest.di.component.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import okhttp3.internal.platform.android.BouncyCastleSocketAdapter.Companion.factory
import javax.inject.Inject

open class TESSAFOLD : Application(), HasAndroidInjector {
    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    override fun onCreate() {
        super.onCreate()
        DaggerAppComponent.builder().application(this).build().inject(this)
    }

    override fun androidInjector(): AndroidInjector<Any> = androidInjector

    val appComponent: AppComponent by lazy {
        initializeComponent()
    }

    open fun initializeComponent(): AppComponent {
        return DaggerAppComponent.builder().build()
    }
}