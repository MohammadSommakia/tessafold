package com.test.tessafoldtest.utils.listeners

interface DialogActionsListener {
    fun onNegativeButtonClicked()
    fun onPositiveButtonClicked()


}