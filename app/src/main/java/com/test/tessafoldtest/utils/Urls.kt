package com.test.tessafoldtest.utils

class Urls {

    companion object {
        const val POSTS = "posts"
        const val USERS = "users"
        const val COMMENTS = "comments"

    }
}