package com.test.tessafoldtest.utils

class Const {

    companion object {
        const val DATABASE_NAME: String = "TESSAFOLD_TEST"
        //Network constants
        const val TIMEOUT_CONNECT = 60L   //In seconds
        const val TIMEOUT_READ = 60L   //In seconds
        const val TIMEOUT_WRITE = 60L   //In seconds

        const val NETWORK_FAILURE = 400
        const val UNKNOWN_ERROR = 500

        const val POST_ID = "POST_ID"

    }
}