package com.test.tessafoldtest.utils.listeners


interface RecyclerItemListener<T> {

    fun onItemClicked(item : T)
}