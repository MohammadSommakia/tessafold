package com.test.tessafoldtest.presentation.post

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.test.tessafoldtest.data.remote.controller.ErrorManager
import com.test.tessafoldtest.data.remote.controller.Resource
import com.test.tessafoldtest.data.remote.responses.GetCommentResponse
import com.test.tessafoldtest.data.remote.responses.GetPostResponse
import com.test.tessafoldtest.data.remote.responses.GetUserResponse
import com.test.tessafoldtest.data.repository.DataRepository
import com.test.tessafoldtest.data.table.Post
import com.test.tessafoldtest.presentation.post.model.PostItem
import com.test.tessafoldtest.utils.Const
import com.test.tessafoldtest.utils.Event
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class PostViewModel @Inject constructor(
    private val dataRepository: DataRepository,
    override val coroutineContext: CoroutineContext,
) : ViewModel(), CoroutineScope {

    val mutableLiveData = MutableLiveData<Event<Resource<List<PostItem>>>>()


    fun loadPosts() {

        mutableLiveData.value = Event(Resource.Loading())
        launch {
            withContext(Dispatchers.IO)
            {
                try {

                val cachedData = dataRepository.getCachedPosts()
                if (cachedData.isEmpty()) {
                    val postsResponse: Resource<List<GetPostResponse>> = dataRepository.getPosts()
                    val usersResponse: Resource<List<GetUserResponse>> = dataRepository.getUsers()
                    val commentsResponse: Resource<List<GetCommentResponse>> = dataRepository.getComments()

                    if (postsResponse is Resource.Success && usersResponse is Resource.Success && commentsResponse is Resource.Success)
                    {
                        saveDataToDB(postsResponse,usersResponse,commentsResponse)
                        prepareData(dataRepository.getCachedPosts())
                    }
                    else
                    {
                        mutableLiveData.postValue(Event(Resource.DataError(Const.NETWORK_FAILURE)))

                    }

                } else
                    prepareData(cachedData)
                }
                    catch (e:Exception)
                    {
                        Log.d("PostViewModel",e.message.toString())
                        mutableLiveData.postValue(Event(Resource.DataError(Const.UNKNOWN_ERROR)))

                    }

            }
        }

    }

    private suspend fun saveDataToDB(
        postsResponse: Resource.Success<List<GetPostResponse>>,
        usersResponse: Resource.Success<List<GetUserResponse>>,
        commentsResponse: Resource.Success<List<GetCommentResponse>>
    ) {
            dataRepository.insertUsers(usersResponse.response)
            dataRepository.insertsPosts(postsResponse.response)
            dataRepository.insertsComments(commentsResponse.response)
    }

    private suspend fun prepareData(post: List<Post>) {
        val list = ArrayList<PostItem>()

        post.forEach { singlePost ->
            val currentUser = dataRepository.getUser(singlePost.userId)
            list.add(
                PostItem(
                    id = singlePost.id,
                    title = singlePost.title,
                    body = singlePost.body,
                    authorId = singlePost.userId,
                    authorName = currentUser.name,
                    authorUserName = currentUser.username
                )
            )
        }

        mutableLiveData.postValue(Event(Resource.Success(list)))
    }
}