package com.test.tessafoldtest.presentation.post.model

class PostItem(
    override val id: Long,
    override val title: String?,
    override val body: String?,
    override val authorId: Long,
    override val authorName: String?,
    override val authorUserName: String?,
    ) : IPostItem