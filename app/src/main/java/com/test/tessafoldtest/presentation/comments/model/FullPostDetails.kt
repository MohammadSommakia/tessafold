package com.test.tessafoldtest.presentation.comments.model

import com.test.tessafoldtest.presentation.post.model.IPostItem

class FullPostDetails (
    override val post: IPostItem?,
    override val user: IUserItem?,
    override val comments: List<ICommentItem>
):IPostComponent