package com.test.tessafoldtest.presentation.post

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.test.tessafoldtest.databinding.ItemPostBinding
import com.test.tessafoldtest.presentation.post.model.IPostItem
import com.test.tessafoldtest.utils.listeners.RecyclerItemListener


class PostAdapter(val listener: RecyclerItemListener<IPostItem>) :
    ListAdapter<IPostItem, PostAdapter.ViewHolder>(PostDiffCallBacks()) {


    override fun getItemCount() = currentList.size


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(currentList[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemPostBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding)
    }


    inner class ViewHolder(private val binding: ItemPostBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {
        fun bind(item: IPostItem) {
            binding.postItem = item
            binding.executePendingBindings()
        }

        init {
            binding.postItemContainer.setOnClickListener(this)
        }

        override fun onClick(view: View?) {
            listener.onItemClicked(currentList[adapterPosition])
        }
    }


    class PostDiffCallBacks : DiffUtil.ItemCallback<IPostItem>() {
        override fun areItemsTheSame(
            oldItem: IPostItem,
            newItem: IPostItem
        ): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(
            oldItem: IPostItem,
            newItem: IPostItem
        ): Boolean {
            return newItem.id == oldItem.id
        }
    }


}