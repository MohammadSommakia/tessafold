package com.test.tessafoldtest.presentation.comments.model

data class CommentItem(
    override val id: Long,
    override val postId: Long,
    override val name: String?,
    override val email: String?,
    override val body: String?
): ICommentItem