package com.test.tessafoldtest.presentation.comments

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.test.tessafoldtest.data.remote.controller.Resource
import com.test.tessafoldtest.data.repository.DataRepository
import com.test.tessafoldtest.data.table.Comment
import com.test.tessafoldtest.data.table.Post
import com.test.tessafoldtest.data.table.User
import com.test.tessafoldtest.presentation.comments.model.*
import com.test.tessafoldtest.presentation.post.model.IPostItem
import com.test.tessafoldtest.utils.Const
import com.test.tessafoldtest.utils.Event
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class CommentsViewModel @Inject constructor(
    private val dataRepository: DataRepository,
    override val coroutineContext: CoroutineContext,
) : ViewModel(), CoroutineScope {

    var fullPostDetailsMutableLiveData: MutableLiveData<Event<Resource<IPostComponent>>> = MutableLiveData()

     var postItem = PostDetails()
     var userItem = UserItem()
    fun getComments(postId: Long) {

        fullPostDetailsMutableLiveData.value = Event(Resource.Loading())
        launch {

            withContext(Dispatchers.IO)
            {
                try {

                    val postDetails = dataRepository.getPostById(postId)
                    val userDetails = dataRepository.getUser(postDetails.userId)
                    val commentsList = dataRepository.getCommentsOfPost(postId)

                    userItem = prepareUserToDisplay(userDetails)
                    postItem = preparePostToDisplay(postDetails,userDetails)
                    val comments = prepareCommentToDisplay(commentsList)

                    val fullPostDetails = FullPostDetails(postItem,userItem,comments)
                    fullPostDetailsMutableLiveData.postValue(Event(Resource.Success(fullPostDetails)))


                }
                catch (e: Exception)
                {
                    Log.d("CommentsViewModel",e.message.toString())
                    fullPostDetailsMutableLiveData.postValue(Event(Resource.DataError(Const.UNKNOWN_ERROR)))

                }
            }
        }


    }

    private fun prepareCommentToDisplay(commentsList: List<Comment>): ArrayList<ICommentItem> {

        val comments = arrayListOf<ICommentItem>()

        commentsList.forEach {
            comment ->
            comments.add(CommentItem(comment.id,comment.postId,comment.name,comment.email,comment.body))
        }

        return comments

    }

    private fun prepareUserToDisplay(userDetails: User): UserItem {
        return UserItem(userDetails.id,userDetails.name,userDetails.username,userDetails.email,userDetails.address?.street,
        userDetails.address?.suite,userDetails.address?.city,userDetails.address?.zipcode,userDetails.phone,
            userDetails.website,userDetails.company?.name,userDetails.company?.bs)
    }

    private fun preparePostToDisplay(postDetails: Post, userDetails: User): PostDetails {
        return PostDetails(postDetails.id,postDetails.title,postDetails.body,userDetails.id,userDetails.name,userDetails.username)

    }
}