package com.test.tessafoldtest.presentation.post

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.test.tessafoldtest.R
import com.test.tessafoldtest.data.remote.controller.Resource
import com.test.tessafoldtest.databinding.FragmentPostsBinding
import com.test.tessafoldtest.presentation.post.model.IPostItem
import com.test.tessafoldtest.presentation.post.model.PostItem
import com.test.tessafoldtest.utils.EventObserver
import com.test.tessafoldtest.utils.ViewModelFactory
import com.test.tessafoldtest.utils.listeners.RecyclerItemListener
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

class PostsFragment : Fragment(), RecyclerItemListener<IPostItem> {

    lateinit var binding: FragmentPostsBinding

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    lateinit var postAdapter: PostAdapter



    private val viewModel: PostViewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(PostViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        if (!this::binding.isInitialized) {
            binding = FragmentPostsBinding.inflate(inflater, container, false)
            binding.viewModel = viewModel
            setupRecyclerView()
            setupObservers()
            viewModel.loadPosts()
        }

        return binding.root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }


    private fun setupObservers() {
        viewModel.mutableLiveData.observe(
            requireActivity(),
            EventObserver
                (object : EventObserver.EventUnhandledContent<Resource<List<PostItem>>> {
                override fun onEventUnhandledContent(t: Resource<List<PostItem>>) {
                    when (t) {
                        is Resource.Loading -> {
                            // show progress bar and remove no data layout while loading
                            binding.postsProgressBar.visibility = View.VISIBLE
                        }
                        is Resource.Success -> {
                            // response is ok get the data and display it in the list
                            binding.postsProgressBar.visibility = View.GONE

                            val response = t.response as List<PostItem>

                            postAdapter.submitList(response)
                        }
                        is Resource.DataError -> {
                            // usually this happening when there is server error
                            Toast.makeText(requireContext(),getString(R.string.unknown_error)
                                , Toast.LENGTH_SHORT).show()
                            binding.postsProgressBar.visibility = View.GONE

                        }
                        is Resource.Exception -> {
                            // usually this happening when there is no internet
                            Toast.makeText(requireContext(),getString(R.string.no_internet)
                                , Toast.LENGTH_SHORT).show()
                            binding.postsProgressBar.visibility = View.GONE
                        }
                    }
                }
            })
        )

    }

    private fun setupRecyclerView() {
        postAdapter = PostAdapter(this)
        binding.postsRecyclerView.adapter = postAdapter

    }

    override fun onItemClicked(item: IPostItem) {
        val action =
            PostsFragmentDirections
                .actionPostsFragmentToCommentsFragment(item.id)
        findNavController().navigate(action)
    }

}