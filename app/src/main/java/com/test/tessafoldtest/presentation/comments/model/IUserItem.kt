package com.test.tessafoldtest.presentation.comments.model

interface IUserItem {

    val id: Long
    val name: String?
    val userName: String?
    val email: String?
    val street: String?
    val suite: String?
    val city: String?
    val zipcode: String?
    val phone: String?
    val website: String?
    val companyName: String?
    val companyBS: String?
}