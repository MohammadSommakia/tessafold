package com.test.tessafoldtest.presentation.comments

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.test.tessafoldtest.R
import com.test.tessafoldtest.data.remote.controller.Resource
import com.test.tessafoldtest.databinding.FragmentCommentBinding
import com.test.tessafoldtest.databinding.ItemPostFullBinding
import com.test.tessafoldtest.databinding.ItemUserDetailsBinding
import com.test.tessafoldtest.presentation.comments.model.ICommentItem
import com.test.tessafoldtest.presentation.comments.model.IPostComponent
import com.test.tessafoldtest.presentation.post.PostAdapter
import com.test.tessafoldtest.utils.Const
import com.test.tessafoldtest.utils.EventObserver
import com.test.tessafoldtest.utils.ViewModelFactory
import com.test.tessafoldtest.utils.listeners.RecyclerItemListener
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject


class CommentsFragment : Fragment(), RecyclerItemListener<ICommentItem> {




    lateinit var binding: FragmentCommentBinding
    lateinit var fullPostBiding: ItemPostFullBinding
    lateinit var userDetailsBiding: ItemUserDetailsBinding

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    lateinit var commentsAdapter: CommentsAdapter



    private val viewModel: CommentsViewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(CommentsViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCommentBinding.inflate(inflater, container, false)
         fullPostBiding = binding.postItem
         userDetailsBiding = binding.userDetailsLayout

        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        setupRecyclerView()
        setupObservers()
        arguments?.getLong(Const.POST_ID)?.let { viewModel.getComments(it) }

        return binding.root
    }

    private fun setupObservers() {

        viewModel.fullPostDetailsMutableLiveData.observe(
            requireActivity(),
            EventObserver
                (object : EventObserver.EventUnhandledContent<Resource<IPostComponent>> {
                override fun onEventUnhandledContent(t: Resource<IPostComponent>) {
                    when (t) {
                        is Resource.Loading -> {
                            // show progress bar and remove no data layout while loading
                            binding.commentsProgressBar.visibility = View.VISIBLE
                            binding.dataContainer.visibility = View.GONE
                        }
                        is Resource.Success -> {
                            // response is ok get the data and display it in the list
                            binding.dataContainer.visibility = View.VISIBLE
                            binding.commentsProgressBar.visibility = View.GONE

                            val response = t.response as IPostComponent
                            fullPostBiding.postItem = response.post
                            userDetailsBiding.userItem = response.user
                            commentsAdapter.submitList(response.comments)
                        }
                        is Resource.DataError -> {
                            Toast.makeText(requireContext(),getString(R.string.unknown_error)
                                , Toast.LENGTH_SHORT).show()
                            binding.commentsProgressBar.visibility = View.GONE
                            binding.dataContainer.visibility = View.GONE

                        }
                        is Resource.Exception -> {
                            Toast.makeText(requireContext(),getString(R.string.unknown_error)
                                , Toast.LENGTH_SHORT).show()
                            binding.commentsProgressBar.visibility = View.GONE
                            binding.dataContainer.visibility = View.GONE
                        }
                    }
                }
            })
        )
    }

    private fun setupRecyclerView() {
        commentsAdapter = CommentsAdapter(this)
        binding.commentsRecyclerView.adapter = commentsAdapter

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    override fun onItemClicked(item: ICommentItem) {


    }

}