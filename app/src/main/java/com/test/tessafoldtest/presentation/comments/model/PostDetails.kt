package com.test.tessafoldtest.presentation.comments.model

import com.test.tessafoldtest.presentation.post.model.IPostItem

class PostDetails (
    override val id: Long, override val title: String?, override val body: String?,
    override val authorId: Long,
    override val authorName: String?,
    override val authorUserName: String?
):IPostItem {
    constructor() : this(
        0,",",",",0,"",""
    )
}