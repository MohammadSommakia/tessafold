package com.test.tessafoldtest.presentation.comments.model

interface ICommentItem {
    val id: Long
    val postId: Long
    val name: String?
    val email: String?
    val body: String?
}