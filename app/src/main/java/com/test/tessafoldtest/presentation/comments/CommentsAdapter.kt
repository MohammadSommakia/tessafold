package com.test.tessafoldtest.presentation.comments

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.test.tessafoldtest.databinding.ItemCommentBinding
import com.test.tessafoldtest.presentation.comments.model.ICommentItem
import com.test.tessafoldtest.utils.listeners.RecyclerItemListener

class CommentsAdapter(val listener: RecyclerItemListener<ICommentItem>) :
    ListAdapter<ICommentItem, CommentsAdapter.ViewHolder>(CommentDiffCallBacks()) {


    override fun getItemCount() = currentList.size


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(currentList[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemCommentBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding)
    }


    inner class ViewHolder(private val binding: ItemCommentBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {
        fun bind(item: ICommentItem) {
            binding.commentItem = item
            binding.executePendingBindings()
        }

        init {
            binding.commentItemContainer.setOnClickListener(this)
        }

        override fun onClick(view: View?) {
            listener.onItemClicked(currentList[adapterPosition])
        }
    }


    class CommentDiffCallBacks : DiffUtil.ItemCallback<ICommentItem>() {
        override fun areItemsTheSame(
            oldItem: ICommentItem,
            newItem: ICommentItem
        ): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(
            oldItem: ICommentItem,
            newItem: ICommentItem
        ): Boolean {
            return newItem.id == oldItem.id
        }
    }
}

