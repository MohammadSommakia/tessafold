package com.test.tessafoldtest.presentation.post.model

interface IPostItem {
    val id: Long
    val title: String?
    val body: String?
    val authorId: Long
    val authorName: String?
    val authorUserName: String?
}