package com.test.tessafoldtest.presentation.comments.model

interface IPostDetails {
    val id: Long
    val title: String?
    val body: String?
}