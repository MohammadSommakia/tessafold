package com.test.tessafoldtest.presentation.comments.model

class UserItem (
    override val id: Long,
    override val name: String?,
    override val userName: String?,
    override val email: String?,
    override val street: String?,
    override val suite: String?,
    override val city: String?,
    override val zipcode: String?,
    override val phone: String?,
    override val website: String?,
    override val companyName: String?,
    override val companyBS: String?
):IUserItem {
    constructor() : this(0,"","","","","","","","","","","")
}