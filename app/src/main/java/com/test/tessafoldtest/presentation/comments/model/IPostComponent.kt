package com.test.tessafoldtest.presentation.comments.model

import com.test.tessafoldtest.presentation.post.model.IPostItem


interface IPostComponent {
    val post: IPostItem?
    val user: IUserItem?
    val comments: List<ICommentItem>
}