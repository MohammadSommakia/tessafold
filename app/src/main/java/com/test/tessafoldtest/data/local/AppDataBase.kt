package com.test.tessafoldtest.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.test.tessafoldtest.data.local.repository.dao.PostDao
import com.test.tessafoldtest.data.local.repository.dao.CommentDao
import com.test.tessafoldtest.data.local.repository.dao.UserDao
import com.test.tessafoldtest.data.table.Comment
import com.test.tessafoldtest.data.table.Post
import com.test.tessafoldtest.data.table.User

@Database(
    entities = [Post::class, Comment::class, User::class],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun postsDao(): PostDao
    abstract fun commentsDao(): CommentDao
    abstract fun usersDao(): UserDao
}