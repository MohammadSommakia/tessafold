package com.test.tessafoldtest.data.table

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey


@Entity(  foreignKeys = [ForeignKey(
    entity = Post::class,
    parentColumns = arrayOf("id"),
    childColumns = arrayOf("postId"),
    onDelete = ForeignKey.CASCADE
)])

class Comment (
    @PrimaryKey
    @ColumnInfo(name = "id")
    var id: Long,

    @ColumnInfo(index = true)
    var postId: Long,

    @ColumnInfo(name = "name")
    var name: String?,

    @ColumnInfo(name = "email")
    var email: String?,

    @ColumnInfo(name = "body")
    var body: String?
)