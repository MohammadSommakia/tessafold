package com.test.tessafoldtest.data.remote.responses

data class GetUserResponse(
    val id: Long,
    val name: String?,
    val username: String?,
    val email: String?,
    val addressResponse: AddressResponse?,
    val phone: String?,
    val website: String?,
    val companyRes: CompanyRes?
)


data class AddressResponse(
    val street: String?,
    val suite: String?,
    val city: String?,
    val zipcode: String?,
    val geo: GeoResponse?
)


data class GeoResponse(
    val lat: Double?,
    val lng: Double?
)


data class CompanyRes(
    val name: String,
    val catchPhrase: String,
    val bs: String
)

