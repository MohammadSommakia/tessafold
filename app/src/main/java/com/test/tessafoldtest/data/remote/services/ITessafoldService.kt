package com.test.tessafoldtest.data.remote.services

import com.test.tessafoldtest.data.remote.responses.GetCommentResponse
import com.test.tessafoldtest.data.remote.responses.GetPostResponse
import com.test.tessafoldtest.data.remote.responses.GetUserResponse
import com.test.tessafoldtest.utils.Urls
import retrofit2.Response
import retrofit2.http.*

interface ITessafoldService {



    @GET(Urls.POSTS)
    suspend fun getPosts(): Response<List<GetPostResponse>>

    @GET(Urls.COMMENTS)
    suspend fun getComments(): Response<List<GetCommentResponse>>

    @GET(Urls.USERS)
    suspend fun getUsers(): Response<List<GetUserResponse>>


}