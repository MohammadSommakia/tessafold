package com.test.tessafoldtest.data.repository

import com.test.tessafoldtest.data.local.repository.LocalRepository
import com.test.tessafoldtest.data.remote.controller.Resource
import com.test.tessafoldtest.data.remote.responses.GetCommentResponse
import com.test.tessafoldtest.data.remote.responses.GetPostResponse
import com.test.tessafoldtest.data.remote.responses.GetUserResponse
import com.test.tessafoldtest.data.table.*
import javax.inject.Inject

class DataRepository
@Inject
constructor(
    private val remoteRepository: RemoteRepository,
    private val localRepository: LocalRepository
) : DataSource {

    override suspend fun getPosts(): Resource<List<GetPostResponse>> {
        return remoteRepository.getPosts()
    }

    override suspend fun getUsers(): Resource<List<GetUserResponse>> {
        return remoteRepository.getUsers()
    }

    override suspend fun getComments(): Resource<List<GetCommentResponse>> {
        return remoteRepository.getComments()
    }

    override suspend fun insertsPosts(posts: List<GetPostResponse>?) {
        val postsList = arrayListOf<Post>()

        posts?.forEach { singlePost ->
            postsList.add(
                Post(
                    id = singlePost.id,
                    userId = singlePost.userId,
                    title = singlePost.title,
                    body = singlePost.body
                )
            )
        }
        localRepository.insertPostsList(postsList)
    }

    override suspend fun insertUsers(users: List<GetUserResponse>?) {
        val usersList = arrayListOf<User>()
        users?.forEach { userRes ->
            usersList.add(
                User(
                    id = userRes.id,
                    name = userRes.name,
                    username = userRes.username,
                    email = userRes.email,
                    phone = userRes.phone,
                    website = userRes.website,
                    Address(
                        street = userRes.addressResponse?.street,
                        suite = userRes.addressResponse?.suite,
                        city = userRes.addressResponse?.city,
                        zipcode = userRes.addressResponse?.zipcode,
                        Coordinate(
                            userRes.addressResponse?.geo?.lat,
                            userRes.addressResponse?.geo?.lng
                        )
                    ),
                    Company(
                        userId = userRes.id,
                        name = userRes.companyRes?.name,
                        catchPhrase = userRes.companyRes?.catchPhrase,
                        bs = userRes.companyRes?.bs
                    )
                )
            )
        }
        localRepository.insertUsersList(usersList)
    }

    override suspend fun insertsComments(comments: List<GetCommentResponse>?) {
        val commentsList = arrayListOf<Comment>()

        comments?.forEach { singleComment ->
            commentsList.add(
                Comment(
                    id = singleComment.id,
                    postId = singleComment.postId,
                    name = singleComment.name,
                    email = singleComment.email,
                    body = singleComment.body
                )
            )
        }
        localRepository.insertCommentsList(commentsList)
    }


    override suspend fun getCachedPosts(): List<Post> {
        return localRepository.getAllPosts()
    }

    override suspend fun getUser(id: Long): User {
        return localRepository.getUserById(userId = id)
    }


    override suspend fun getCommentsOfPost(postId: Long): List<Comment> {
        return localRepository.getCommentsOfPost(postId = postId)
    }

    override suspend fun getPostById(postId: Long): Post {
        return localRepository.getPostById(postId)
    }

}

