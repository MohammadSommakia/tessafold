package com.test.tessafoldtest.data.remote.controller


// A generic class that contains data and status about loading this data.
sealed class Resource<T>(
    val response: T?,
    val error: Any? = null
) {
    class Success<T>(response: T) : Resource<T>(response)
    class Loading<T>(data: T? = null) : Resource<T>(data)
    class DataError<T>(errorCode: Int) : Resource<T>(null, errorCode)
    class Exception<T>(errorMessage: T) : Resource<T>(errorMessage)
}