package com.test.tessafoldtest.data.repository

import com.test.tessafoldtest.data.remote.controller.Resource
import com.test.tessafoldtest.data.table.Comment
import com.test.tessafoldtest.data.table.Post
import com.test.tessafoldtest.data.table.User
import com.test.tessafoldtest.data.remote.responses.GetCommentResponse
import com.test.tessafoldtest.data.remote.responses.GetPostResponse
import com.test.tessafoldtest.data.remote.responses.GetUserResponse

interface DataSource {


    suspend fun getPosts(): Resource<List<GetPostResponse>>
    suspend fun getUsers(): Resource<List<GetUserResponse>>
    suspend fun getComments(): Resource<List<GetCommentResponse>>


    suspend fun insertsPosts(posts: List<GetPostResponse>?)
    suspend fun insertUsers(users: List<GetUserResponse>?)
    suspend fun insertsComments(comments: List<GetCommentResponse>?)


    suspend fun  getCachedPosts(): List<Post>
    suspend fun getUser(id: Long): User
    suspend fun getCommentsOfPost(postId: Long): List<Comment>
    suspend fun getPostById(postId: Long): Post

}