package com.test.tessafoldtest.data.repository

import com.google.gson.Gson
import com.test.tessafoldtest.data.remote.controller.ErrorManager
import com.test.tessafoldtest.data.remote.controller.IRestApiManager
import com.test.tessafoldtest.data.remote.controller.Resource
import com.test.tessafoldtest.data.remote.controller.ServiceGenerator
import com.test.tessafoldtest.data.remote.responses.GetCommentResponse
import com.test.tessafoldtest.data.remote.services.ITessafoldService
import com.test.tessafoldtest.data.remote.responses.GetPostResponse
import com.test.tessafoldtest.data.remote.responses.GetUserResponse
import retrofit2.HttpException
import java.io.IOException
import java.lang.Exception
import java.net.SocketTimeoutException
import javax.inject.Inject


class RemoteRepository
@Inject constructor(private val serviceGenerator: ServiceGenerator, private val gson: Gson) :
    IRestApiManager {

    @Suppress("BlockingMethodInNonBlockingContext")

    override suspend fun getPosts(): Resource<List<GetPostResponse>> {
        val authService = serviceGenerator.createService(ITessafoldService::class.java)
        return try {
            val response = authService.getPosts()

            if (response.isSuccessful) {
                Resource.Success(response.body() as List<GetPostResponse>)
            } else {
                Resource.DataError(response.code())
            }
        } catch (e: Exception) {
            Resource.Exception(arrayListOf())
        }
    }

    override suspend fun getComments(): Resource<List<GetCommentResponse>> {
        val authService = serviceGenerator.createService(ITessafoldService::class.java)
        return try {
            val response = authService.getComments()

            if (response.isSuccessful) {
                Resource.Success(response.body() as List<GetCommentResponse>)
            } else {

                Resource.DataError(response.code())
            }
        }  catch (e: Exception) {
            Resource.Exception(arrayListOf())
        }

    }

    override suspend fun getUsers(): Resource<List<GetUserResponse>> {
        val authService = serviceGenerator.createService(ITessafoldService::class.java)
        return try {
            val response = authService.getUsers()

            if (response.isSuccessful) {
                Resource.Success(response.body() as List<GetUserResponse>)
            } else {
                Resource.DataError(response.code())
            }
        } catch (e: Exception) {
            Resource.Exception(arrayListOf())
        }
    }
}