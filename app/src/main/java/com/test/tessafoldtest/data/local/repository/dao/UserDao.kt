package com.test.tessafoldtest.data.local.repository.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.test.tessafoldtest.data.table.User

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(comments: ArrayList<User>): LongArray

    @Query("SELECT * FROM User WHERE id = :id")
    suspend fun getUser(id: Long): User


    @Query("DELETE FROM User")
    suspend fun deleteAll()
}