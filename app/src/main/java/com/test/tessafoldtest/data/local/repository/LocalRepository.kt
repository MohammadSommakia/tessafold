package com.test.tessafoldtest.data.local.repository

import com.test.tessafoldtest.data.local.repository.dao.PostDao
import com.test.tessafoldtest.data.local.repository.dao.CommentDao
import com.test.tessafoldtest.data.local.repository.dao.UserDao
import com.test.tessafoldtest.data.table.Comment
import com.test.tessafoldtest.data.table.Post
import com.test.tessafoldtest.data.table.User
import javax.inject.Inject

class LocalRepository
@Inject constructor(
    private val postDao: PostDao, private val commentDao: CommentDao, private val userDao : UserDao
) {


    suspend fun insertPostsList(posts: ArrayList<Post>): LongArray {
        return postDao.insertAll(posts)
    }


    suspend fun insertCommentsList(comments: ArrayList<Comment>): LongArray {
        return commentDao.insertAll(comments)
    }


    suspend fun insertUsersList(users: ArrayList<User>): LongArray {
        return userDao.insertAll(users)
    }

    suspend fun getAllPosts() : List<Post>
    {
        return postDao.getPosts()

    }

    suspend fun getCommentsOfPost(postId: Long): List<Comment> {
        return commentDao.getCommentsOfPost(postId)
    }

    suspend fun getUserById(userId: Long): User {
        return userDao.getUser(userId)
    }

    suspend fun getPostById(postId: Long): Post {

        return postDao.getPostById(postId)
    }


}