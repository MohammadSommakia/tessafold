package com.test.tessafoldtest.data.table

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "User")
class User(

    @PrimaryKey
    @ColumnInfo(name = "id")
    var id: Long,

    @ColumnInfo(name = "name")
    var name: String?,

    @ColumnInfo(name = "username")
    var username: String?,

    @ColumnInfo(name = "email")
    var email: String?,

    @ColumnInfo(name = "phone")
    var phone: String?,
    @ColumnInfo(name = "website")
    var website: String?,

    @Embedded val address: Address?,

    @Embedded val company: Company?


)
