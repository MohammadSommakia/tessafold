package com.test.tessafoldtest.data.table

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey


@Entity(tableName = "Coordinate")


class Coordinate(


    @ColumnInfo(name = "lat")
    val lat: Double?,

    @ColumnInfo(name = "lng")
    val lng: Double?
)
{
    @PrimaryKey
    @ColumnInfo(name = "geoId")
    var id: Long = 0
}
