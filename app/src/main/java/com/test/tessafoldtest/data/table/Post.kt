package com.test.tessafoldtest.data.table

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(foreignKeys = [ForeignKey(
    entity = User::class,
    parentColumns = arrayOf("id"),
    childColumns = arrayOf("userId"),
    onDelete = ForeignKey.CASCADE
)])

class Post (
    @PrimaryKey
    @ColumnInfo(name = "id")
    var id: Long,

    @ColumnInfo(index = true)
    var userId: Long,

    @ColumnInfo(name = "title")
    var title: String?,

    @ColumnInfo(name = "body")
    var body: String?
)