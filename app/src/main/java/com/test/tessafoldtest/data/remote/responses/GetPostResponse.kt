package com.test.tessafoldtest.data.remote.responses


data class GetPostResponse(

    val id: Long,
    val userId: Long,
    val title: String,
    val body: String

)



