package com.test.tessafoldtest.data.table

import androidx.room.*

@Entity(tableName = "Address")


class Address(


    @ColumnInfo(name = "street")
    val street: String?,

    @ColumnInfo(name = "suite")
    val suite: String?,

    @ColumnInfo(name = "city")
    val city: String?,

    @ColumnInfo(name = "zipcode")
    val zipcode: String?,

    @Embedded  val geo: Coordinate?
)
{
    @PrimaryKey
    @ColumnInfo(name = "addressId")
    var id: Long = 0
}