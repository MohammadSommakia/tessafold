package com.test.tessafoldtest.data.local.repository.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.test.tessafoldtest.data.table.Comment


@Dao
interface CommentDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(comments: ArrayList<Comment>): LongArray


    @Query("SELECT * FROM Comment WHERE postId = :postId")
    suspend fun getCommentsOfPost(postId: Long): List<Comment>


    @Query("DELETE FROM Comment")
    suspend fun deleteAll()

}