package com.test.tessafoldtest.data.local.repository.dao

import androidx.room.*
import com.test.tessafoldtest.data.table.Post

@Dao
interface PostDao {


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(posts: ArrayList<Post>): LongArray

    @Query("SELECT * FROM Post")
    suspend fun getPosts(): List<Post>

    @Query("DELETE FROM Post")
    suspend fun deleteAll()


    @Query("SELECT * FROM Post WHERE id =:postId")
    suspend fun getPostById(postId: Long): Post

}