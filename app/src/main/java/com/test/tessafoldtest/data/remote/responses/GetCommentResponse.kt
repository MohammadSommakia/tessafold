package com.test.tessafoldtest.data.remote.responses

class GetCommentResponse(

    val id: Long,
    val postId: Long,
    val name: String,
    val email: String,
    val body: String

)
