package com.test.tessafoldtest.data.remote.controller

import com.test.tessafoldtest.data.remote.responses.GetCommentResponse
import com.test.tessafoldtest.data.remote.responses.GetPostResponse
import com.test.tessafoldtest.data.remote.responses.GetUserResponse

internal interface IRestApiManager {

    suspend fun getPosts(): Resource<List<GetPostResponse>>
    suspend fun getComments(): Resource<List<GetCommentResponse>>
    suspend fun getUsers(): Resource<List<GetUserResponse>>

}