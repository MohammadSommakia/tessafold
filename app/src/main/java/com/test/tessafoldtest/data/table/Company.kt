package com.test.tessafoldtest.data.table

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "Company")


class Company(

    @ColumnInfo(index = true)
    var userId: Long,

    @ColumnInfo(name = "companyName")
    val name: String?,

    @ColumnInfo(name = "catchPhrase")
    val catchPhrase: String?,

    @ColumnInfo(name = "bs")
    val bs: String?
)
{
    @PrimaryKey
    @ColumnInfo(name = "companyId")
    var id: Long = 0

}
