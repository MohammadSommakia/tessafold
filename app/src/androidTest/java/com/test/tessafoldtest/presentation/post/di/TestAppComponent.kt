package com.test.tessafoldtest.presentation.post.di

import android.app.Application
import android.content.Context
import com.test.tessafoldtest.di.component.AppComponent
import com.test.tessafoldtest.di.module.*
import com.test.tessafoldtest.presentation.post.DefaultDataRepositoryTest
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton




@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        TestAppModule::class,
    ]
)
interface TestAppComponent : AppComponent {
    @Component.Factory
    interface Factory {
        fun create(@BindsInstance applicationContext: Context): TestAppComponent
        @BindsInstance
        fun application(application: Application): AppComponent.Builder
    }

    fun inject(app: MyTestApplication)

}


