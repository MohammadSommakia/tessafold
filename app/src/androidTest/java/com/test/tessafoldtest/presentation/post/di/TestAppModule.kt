package com.test.tessafoldtest.presentation.post.di

import android.app.Application
import androidx.room.Room
import com.google.gson.Gson
import com.test.tessafoldtest.data.local.AppDatabase
import com.test.tessafoldtest.data.repository.DataRepository
import com.test.tessafoldtest.data.repository.DataSource
import com.test.tessafoldtest.utils.Const
import dagger.Binds
import dagger.Module
import dagger.Provides
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import javax.inject.Singleton
import kotlin.coroutines.CoroutineContext

@Module
 class TestAppModule {
    @Singleton
    @Provides
    fun provideTasksRemoteDataSource(
        apiService: DataRepository
    ): DataSource {
        return mockk()
    }

//    @Binds
//    @Provides
//     fun provideDataRepository(dataRepository: DataRepository): DataSource
//    {return mockk()}

    @Singleton
    @Provides
    fun provideDatabase(
        app: MyTestApplication
    ) = Room.databaseBuilder(
        app,
        AppDatabase::class.java,
        Const.DATABASE_NAME
    ).build()

    @Singleton
    @Provides
    fun providePostsDao(db: AppDatabase) = db.postsDao()

    @Singleton
    @Provides
    fun provideCommentsDao(db: AppDatabase) = db.commentsDao()

    @Singleton
    @Provides
    fun provideUsersDao(db: AppDatabase) = db.usersDao()


    @Provides
    @Singleton
    fun provideCoroutineContext(): CoroutineContext {
        return Dispatchers.Main
    }

    @Provides
    @Singleton
    fun provideGsonObject(): Gson {
        return Gson()
    }

}