package com.test.tessafoldtest.presentation.post.di

import com.test.tessafoldtest.TESSAFOLD
import com.test.tessafoldtest.di.component.AppComponent

class MyTestApplication : TESSAFOLD() {

        override fun initializeComponent(): AppComponent {
            // Creates a new TestAppComponent that injects fakes types
            return DaggerTestAppComponent.factory().create(this)
        }
    }
