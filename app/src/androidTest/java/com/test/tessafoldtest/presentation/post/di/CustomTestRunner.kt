package com.test.tessafoldtest.presentation.post.di

import android.app.Application
import android.content.Context
import androidx.test.runner.AndroidJUnitRunner
import com.test.tessafoldtest.TESSAFOLD

class CustomTestRunner : AndroidJUnitRunner() {
    override fun newApplication(cl: ClassLoader?, name: String?, context: Context?): Application {
        return super.newApplication(cl, MyTestApplication::class.java.name, context)
    }
}