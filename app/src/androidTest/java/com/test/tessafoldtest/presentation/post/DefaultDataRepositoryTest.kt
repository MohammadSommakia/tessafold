package com.test.tessafoldtest.presentation.post

import com.test.tessafoldtest.data.local.repository.LocalRepository
import com.test.tessafoldtest.data.remote.responses.GetPostResponse
import com.test.tessafoldtest.data.repository.DataRepository
import com.test.tessafoldtest.data.repository.DataSource
import com.test.tessafoldtest.data.repository.RemoteRepository
import com.test.tessafoldtest.presentation.post.di.DaggerTestAppComponent
import com.test.tessafoldtest.presentation.post.di.MyTestApplication
import com.test.tessafoldtest.presentation.post.di.TestAppComponent
import io.mockk.coEvery
import io.mockk.coVerify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Test
import javax.inject.Inject

class DefaultDataRepositoryTest {

    @Inject
    lateinit var dataRepository: DataRepository



    @Before
    fun setup() {
//        val component: TestAppComponent = DaggerTestAppComponent.factory()
//            .create(MyTestApplication())
//        component.inject(this)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun getPostsResponse_testMethodCall() = runBlockingTest {

        dataRepository.getPosts()
        coVerify { dataRepository.getPosts() }

        coEvery { dataRepository.getPosts().response } returns listOf<GetPostResponse>()
        dataRepository.getPosts()
        coVerify { dataRepository.getPosts() }
    }
}